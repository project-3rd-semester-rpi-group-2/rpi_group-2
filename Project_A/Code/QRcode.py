from tkinter import *
from tkinter import messagebox
import pyqrcode

# Setting names and config for the tkinter popup
ws = Tk() 
ws.title("RPi Queue - Teaam 2") # name of the pop up window
ws.config(bg='#20c1c9') # standard hex code for colour of the tkinter pop up window
ws.geometry("400x400") # size of the window

# Defines the destination of the QR code
site = "http://192.168.8.200:1880/ui/#!/0?socketid=cGLak-BOraSE6qvbAAAh"


def generate_QR():
    global qr,img
    # Opens the QR code
    qr = pyqrcode.create(site)
    # Set the size of the qrcode
    img = BitmapImage(data = qr.xbm(scale=6))
    display_code()
    

def display_code():
    img_lbl.config(image = img)
    
button = Button(
    ws,
    text = "generate_QR", # name of the button
    width=15, # width of the button 
    command = generate_QR # when button pressed, generates QR
    )
button.pack(pady=20) # Sets the amount of padding around the button. 

img_lbl = Label(
    ws,
    bg='#20c1c9') # sets the background of the QR code
img_lbl.pack()
output = Label(
    ws,
    text="", # You can put text, set this to be the same as background, otherwise end up with white bar instead
    bg='#20c1c9' #standard hex code for the color of the label
    )
output.pack()
 
ws.mainloop()
