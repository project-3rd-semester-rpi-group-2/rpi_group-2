[
    {
        "id": "8495339f2dfb892c",
        "type": "tab",
        "label": "Flow 1",
        "disabled": false,
        "info": "",
        "env": []
    },
    {
        "id": "93c5b13c803a9fab",
        "type": "function",
        "z": "8495339f2dfb892c",
        "name": "Counter",
        "func": "\nvar counter = flow.get(\"counter\") || 0\n\ncounter ++\nif (counter > 99) {\n    counter = 0 \n}\n\nflow.set(\"counter\", counter)\n\nmsg.payload = counter\n\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "initialize": "",
        "finalize": "",
        "libs": [],
        "x": 460,
        "y": 160,
        "wires": [
            [
                "49bf6702ca280e33"
            ]
        ]
    },
    {
        "id": "49bf6702ca280e33",
        "type": "ui_text",
        "z": "8495339f2dfb892c",
        "group": "09e6ff28d82766c7",
        "order": 2,
        "width": 0,
        "height": 0,
        "name": "",
        "label": "Position in queue : ",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "className": "",
        "x": 670,
        "y": 160,
        "wires": []
    },
    {
        "id": "d4e7d01252be976e",
        "type": "ui_text",
        "z": "8495339f2dfb892c",
        "group": "09e6ff28d82766c7",
        "order": 3,
        "width": 0,
        "height": 0,
        "name": "",
        "label": "Wait time (in minutes) : ",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "className": "",
        "x": 680,
        "y": 280,
        "wires": []
    },
    {
        "id": "7ccecb59b37a7222",
        "type": "function",
        "z": "8495339f2dfb892c",
        "name": "qtime",
        "func": "\nvar qtime = flow.get(\"qtime\") || 0\n\nqtime = qtime + 1.5\n\nif (qtime > 148.5) {\n    qtime = 0\n}\n\nflow.set(\"qtime\", qtime)\n\nmsg.payload = qtime\n\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "initialize": "",
        "finalize": "",
        "libs": [],
        "x": 450,
        "y": 280,
        "wires": [
            [
                "d4e7d01252be976e"
            ]
        ]
    },
    {
        "id": "20a316d2629239ed",
        "type": "ui_button",
        "z": "8495339f2dfb892c",
        "name": "",
        "group": "09e6ff28d82766c7",
        "order": 1,
        "width": 0,
        "height": 0,
        "passthru": false,
        "label": "Click to be added to the queue",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "className": "",
        "icon": "",
        "payload": "",
        "payloadType": "str",
        "topic": "topic",
        "topicType": "msg",
        "x": 170,
        "y": 160,
        "wires": [
            [
                "93c5b13c803a9fab",
                "7ccecb59b37a7222"
            ]
        ]
    },
    {
        "id": "9e62dd407ecf1a6b",
        "type": "ui_button",
        "z": "8495339f2dfb892c",
        "name": "",
        "group": "09e6ff28d82766c7",
        "order": 3,
        "width": 0,
        "height": 0,
        "passthru": false,
        "label": "Reset",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "className": "",
        "icon": "",
        "payload": "",
        "payloadType": "str",
        "topic": "topic",
        "topicType": "flow",
        "x": 90,
        "y": 280,
        "wires": [
            [
                "7935133ee6cf2872"
            ]
        ]
    },
    {
        "id": "7935133ee6cf2872",
        "type": "function",
        "z": "8495339f2dfb892c",
        "name": "Reset function",
        "func": "flow.set(\"counter\",-1)\nflow.set(\"qtime\",-1.5)\nreturn { payload: { unit: true } };",
        "outputs": 1,
        "noerr": 0,
        "initialize": "",
        "finalize": "",
        "libs": [],
        "x": 240,
        "y": 280,
        "wires": [
            [
                "93c5b13c803a9fab",
                "7ccecb59b37a7222"
            ]
        ]
    },
    {
        "id": "09e6ff28d82766c7",
        "type": "ui_group",
        "name": "Welcome to the Queue",
        "tab": "40988f14189fdfa8",
        "order": 1,
        "disp": true,
        "width": "6",
        "collapse": false,
        "className": ""
    },
    {
        "id": "40988f14189fdfa8",
        "type": "ui_tab",
        "name": "Project 3rd semester - RPi Group 2",
        "icon": "dashboard",
        "disabled": false,
        "hidden": false
    }
]
