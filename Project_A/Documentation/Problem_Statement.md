# Problem Statement 

The problem with the current solution is that the systems are producing massive amounts of paper waste a year. This is due to customers pulling a ticket with a number on it, which shows their place in the queue. The user then throws the ticket away and remembers the number. 
This problem was first seen when people found a solution to the generic waiting in queue system. The paper roll system was brought about to combat this problem. 
The problem was occurring in a wide variety of shops and businesses, anywhere where the customer would have to queue for their chance to purchase or consult a member of staff. 
The problem was affecting customers and businesses. The customers were waiting a long time for their chance to collect, purchase or talk to someone, while the staff were having to replace these rolls of paper potentially multiple times a day. Meanwhile the businesses were wasting money on multiple paper rolls annually. 
The sequence of events that led to the paper roll system coming into place, was the fact that customers were lining up in large queues with no clue as to how long they would have to wait. 
- How to reduce the amount of paper that companies are wasting annually? 
- How to make a user interface that all users will be able to navigate and use? 
- How to show users their place in the queue in an easy and simple manner? 
