# RPI_Group 2 



## Members

Reuben Badham       @Reuben_Badham <br>
Oliver              @Oliver47890 <br>
Christian Mogensen  @rivvi <br>
Jonas Nissen        @jjffnn

## About the project

The RPi Queue project is designed to replace the current paper roll ticket system that many pharmacies, bakeries and package shops implement all over Denmark and many other countries. 
The team will had to create a system that ran on a raspberry pi, that showed a QR code. Once the QR code was scanned the user would be redirected to a node red dashboard where they could see their place number in the queue and be told an estimated time until their number was called. <br>
An optional additional deliverable was to have the QR code refresh every couple of minutes to stop people abusing the system.  

Project B has the team refine and adjust the system to a stage that it could be called the "final product". 
The team has decided to make the queue system on a python flask web application. The web application will be hosted in the cloud using Google Cloud Run. This should hopefully elleviate some of the issues with the previous system and provide some security to the application. 
